from NetworkStatisticsService.latency_statistic_utils import LatencyStatisticUtils
import unittest
from NetworkStatisticsService.exceptions import LatencyStatisticsException, CommandOutputFormatException


class LatencyStatisticUtilsTest(unittest.TestCase):

	def test_get_packet_loss_from_command_output_complete_input(self):
		given_input = '3 packets transmitted, 3 packets received, 0.0% packet loss'
		expected_output = {'Loss': 0}
		actual_output = LatencyStatisticUtils._LatencyStatisticUtils__get_packet_loss_from_command_output(given_input)
		self.assertEqual(expected_output, actual_output)

	
	def test_get_packet_loss_from_command_output_complete_input_rasp_version(self):
		given_input = '3 packets transmitted, 3 packets received, 0.0% packet loss, time 4006ms'
		expected_output = {'Loss': 0}
		actual_output = LatencyStatisticUtils._LatencyStatisticUtils__get_packet_loss_from_command_output(given_input)
		self.assertEqual(expected_output, actual_output)


	def test_get_packet_loss_from_command_output_wrong_input(self):
		given_input = 'Some error occured.'
		self.assertRaises(CommandOutputFormatException, LatencyStatisticUtils._LatencyStatisticUtils__get_packet_loss_from_command_output, given_input)


	def test_get_ping_statistics_from_command_output_complete_input(self):
		given_input = 'round-trip min/avg/max/stddev = 17.746/102.731/145.647/60.094 ms'
		expected_output = {'Min': 17.746, 'Avg': 102.731, 'Max': 145.647, 'Stddev': 60.094}
		actual_output = LatencyStatisticUtils._LatencyStatisticUtils__get_ping_statistics_from_command_output(given_input)
		self.assertEqual(expected_output, actual_output)


	def test_get_ping_statistics_from_command_output_wrong_input(self):
		given_input = 'round-trip min/avg/max/stddev 02.731/145.647/60.094 ms'
		self.assertRaises(CommandOutputFormatException, LatencyStatisticUtils._LatencyStatisticUtils__get_ping_statistics_from_command_output, given_input)


	def test_extract_latency_statistics_from_command_output_default(self):
		given_input = ['3 packets transmitted, 3 packets received, 0.0% packet loss',
                       'round-trip min/avg/max/stddev = 17.746/102.731/145.647/60.094 ms']
		expected_output = {'Loss': 0, 'Min': 17.746,
                           'Avg': 102.731, 'Max': 145.647, 'Stddev': 60.094}
		actual_output = LatencyStatisticUtils.extract_latency_statistics_from_command_output(given_input)
		self.assertEqual(expected_output, actual_output)

	def test_extract_latency_statistics_from_command_output_only_loss(self):
		given_input = [
            '3 packets transmitted, 3 packets received, 0.0% packet loss']
		self.assertRaises(LatencyStatisticsException, LatencyStatisticUtils.extract_latency_statistics_from_command_output, given_input)

	def test_extract_latency_statistics_from_command_output_only_latency(self):
		given_input = [
            'round-trip min/avg/max/stddev = 17.746/102.731/145.647/60.094 ms']
		self.assertRaises(LatencyStatisticsException, LatencyStatisticUtils.extract_latency_statistics_from_command_output, given_input)
	
	
	def test_extract_latency_statistics_from_command_output_wrong_latency(self):
		given_input = ['3 packets transmitted, 3 packets received, 0.0% packet loss',
                       'round-trip min/avg/max/ 1/145.647/60.094 ms']
		self.assertRaises(LatencyStatisticsException, LatencyStatisticUtils.extract_latency_statistics_from_command_output, given_input)

	def test_extract_latency_statistics_from_command_output_wrong_loss(self):
		given_input = ['Errors occured',
                       'round-trip min/avg/max/stddev = 17.746/102.731/145.647/60.094 ms']
		self.assertRaises(LatencyStatisticsException, LatencyStatisticUtils.extract_latency_statistics_from_command_output, given_input)

	def test_extract_latency_statistics_from_command_output_no_input(self):
		given_input = []
		self.assertRaises(LatencyStatisticsException, LatencyStatisticUtils.extract_latency_statistics_from_command_output, given_input)
