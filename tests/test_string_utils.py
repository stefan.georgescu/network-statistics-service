import unittest

from NetworkStatisticsService.string_utils import StringUtils

class StringUtilsTest(unittest.TestCase):

    def test_remove_empty_strings_from_string_list_default(self):
        given_input = ['', 'John', '', 'Appleseed']
        expected_output = ['John', 'Appleseed']
        actual_output = StringUtils.remove_empty_strings_from_string_list(given_input)
        self.assertEqual(actual_output, expected_output)


    def test_remove_empty_strings_from_string_list_no_empty_strings(self):
        given_input = ['John', 'Appleseed']
        expected_output = ['John', 'Appleseed']
        actual_output = StringUtils.remove_empty_strings_from_string_list(given_input)
        self.assertEqual(actual_output, expected_output)


    def test_remove_empty_strings_from_string_list_only_empty_strings(self):
        given_input = ['', '']
        expected_output = []
        actual_output = StringUtils.remove_empty_strings_from_string_list(given_input)
        self.assertEqual(actual_output, expected_output)


    def test_remove_empty_strings_from_string_list_empty_list(self):
        given_input = []
        expected_output = []
        actual_output = StringUtils.remove_empty_strings_from_string_list(given_input)
        self.assertEqual(actual_output, expected_output)